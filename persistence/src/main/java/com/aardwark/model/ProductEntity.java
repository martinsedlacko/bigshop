package com.aardwark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private BigDecimal price;

    @Column
    private String name;

    @Column
    private Integer ammount;

    @Column
    private String description;

    @Column
    private byte[] image;

    @Column
    private Double vat;

    @Column
    private String type;

    @Column
    private BigDecimal discount;
}
