package com.aardwark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String email;

    @Column
    private String lastName;

    @Column
    private String firstName;

    @Column
    private String telephoneNumber;

    @OneToOne
    @JoinColumn(name = "billing_address_id" , referencedColumnName = "id")
    private AddressEntity billingAddress;

    @OneToOne
    @JoinColumn(name = "delivery_address_id", referencedColumnName = "id")
    private AddressEntity deliveryAddress;
}
