package com.aardwark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private List<ProductEntity> products;

    @Column
    private BigDecimal nettoPrice;

    @Column
    private BigDecimal bruttoPrice;

    @Column
    private String sentAt;

    @Column
    private String deliveredAt;

    @OneToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private ClientEntity client;

    @Column
    private BigDecimal discount;
}
