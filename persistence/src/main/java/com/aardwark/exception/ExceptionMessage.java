package com.aardwark.exception;

public class ExceptionMessage {

    private ExceptionMessage() {
    }

    private static final String NOT_FOUND = " not found";

    public static String getNotFoundMessage(String what){
        return what.concat(NOT_FOUND);
    }
}
