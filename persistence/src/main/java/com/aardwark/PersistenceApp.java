package com.aardwark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@PropertySources({
        @PropertySource("application.properties"),
        @PropertySource("database.properties")
})
public class PersistenceApp {

    public static void main(String[] args) {
        SpringApplication.run(PersistenceApp.class, args);
    }
}
