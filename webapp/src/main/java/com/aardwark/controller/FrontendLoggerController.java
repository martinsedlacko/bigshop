package com.aardwark.controller;

import com.aardwark.model.dto.FrontendLogDto;
import com.aardwark.model.dto.FrontendLogsDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/")
public class FrontendLoggerController {

    private static final Logger logger = LoggerFactory.getLogger(FrontendLoggerController.class.getName());

    @PostMapping(value = "logger", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void log(@RequestBody FrontendLogsDto frontendLogsDto) {
        for(FrontendLogDto log : frontendLogsDto.getLogs()){
            switch (log.getLevel()){
                case "info" : logger.info(log.getMessage()); break;
                case "error" : logger.error(log.getMessage()); break;
                case "warn" : logger.warn(log.getMessage()); break;
                case "trace" : logger.trace(log.getMessage());break;
                default:
            }
        }
    }
}
