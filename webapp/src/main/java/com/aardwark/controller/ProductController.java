package com.aardwark.controller;

import com.aardwark.generated.api.ProductApi;
import com.aardwark.generated.model.Product;
import com.aardwark.mapper.ProductMapper;
import com.aardwark.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController implements ProductApi {
    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class.getName());

    ProductService productService;
    ProductMapper productMapper ;

    @Autowired
    public void setProductMapper(ProductMapper productMapper){
        this.productMapper = productMapper;
    }

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public ResponseEntity<List<Product>> findAllProducts() {
        return ResponseEntity.ok(
                productService.findAll()
                .stream()
                .map(productMapper::convertToApiModel)
                .collect(Collectors.toList())
        );
    }
}
