package com.aardwark.controller;

import com.aardwark.generated.api.OrderApi;
import com.aardwark.generated.model.Order;
import com.aardwark.generated.model.OrderDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController implements OrderApi {

    @Override
    public ResponseEntity<Order> addOrder(OrderDto order) {
        return OrderApi.super.addOrder(order);
    }
}
