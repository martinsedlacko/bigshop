package com.aardwark.mapper;

import com.aardwark.generated.model.Address;
import com.aardwark.model.AddressEntity;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AddressMapper extends AbstractMapper {

    public AddressEntity convertToEntity(Address address){
        return AddressEntity.builder()
                .id(address.getId())
                .city(address.getCity())
                .postCode(address.getPostCode())
                .street(address.getStreet())
                .streetNumber(address.getStreetNumber())
                .country(address.getCountry())
                .build();
    }

    public Address convertToApiModel(AddressEntity addressEntity){
        return new Address()
                .id(addressEntity.getId())
                .city(addressEntity.getCity())
                .postCode(addressEntity.getPostCode())
                .street(addressEntity.getStreet())
                .streetNumber(addressEntity.getStreetNumber())
                .country(addressEntity.getCountry());
    }
}

