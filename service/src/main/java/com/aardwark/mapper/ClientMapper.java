package com.aardwark.mapper;

import com.aardwark.generated.model.Address;
import com.aardwark.generated.model.Client;
import com.aardwark.model.ClientEntity;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class ClientMapper extends AbstractMapper{

    private AddressMapper addressMapper;

    public ClientMapper(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    public ClientEntity convertToEntity(Client client){
        return ClientEntity.builder()
                .id(client.getId())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .deliveryAddress(addressMapper.convertToEntity(client.getDeliveryAddress()))
                .billingAddress(addressMapper.convertToEntity(client.getBillingAddress()))
                .email(client.getEmail())
                .telephoneNumber(client.getTelephoneNumber())
                .build();
    }

    public Client convertToApiModel(ClientEntity clientEntity){
        return new Client()
                .id(clientEntity.getId())
                .firstName(clientEntity.getFirstName())
                .lastName(clientEntity.getLastName())
                .deliveryAddress(addressMapper.convertToApiModel(clientEntity.getDeliveryAddress()))
                .billingAddress(addressMapper.convertToApiModel(clientEntity.getBillingAddress()))
                .email(clientEntity.getEmail())
                .telephoneNumber(clientEntity.getTelephoneNumber());
    }
}
