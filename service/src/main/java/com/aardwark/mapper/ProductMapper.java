package com.aardwark.mapper;

import com.aardwark.generated.model.Product;
import com.aardwark.model.ProductEntity;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProductMapper extends AbstractMapper {

    public Product convertToApiModel(ProductEntity productEntity){
        return new Product()
                .id(productEntity.getId())
                .price(productEntity.getPrice())
                .name(productEntity.getName())
                .ammount(productEntity.getAmmount())
                .description(productEntity.getDescription())
                .image(productEntity.getImage())
                .vat(productEntity.getVat())
                .type(productEntity.getType())
                .discount(productEntity.getDiscount());
    }

    public ProductEntity convertToEntity(Product product){
        return ProductEntity.builder()
                .id(product.getId())
                .price(product.getPrice())
                .name(product.getName())
                .ammount(product.getAmmount())
                .description(product.getDescription())
                .image(product.getImage())
                .vat(product.getVat())
                .type(product.getType())
                .discount(product.getDiscount())
                .build();
    }
}
