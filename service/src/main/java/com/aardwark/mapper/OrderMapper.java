package com.aardwark.mapper;

import com.aardwark.generated.model.Order;
import com.aardwark.generated.model.Product;
import com.aardwark.model.OrderEntity;
import com.aardwark.model.ProductEntity;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class OrderMapper extends AbstractMapper{
    private ClientMapper clientMapper;
    private ProductMapper productMapper;

    public OrderMapper(ClientMapper clientMapper, ProductMapper productMapper) {
        this.clientMapper = clientMapper;
        this.productMapper = productMapper;
    }

    public OrderEntity convertToEntity(Order order){
        List<ProductEntity> products = order.getProducts().stream()
                .map(product -> productMapper.convertToEntity(product))
                .collect(Collectors.toList());

        return OrderEntity.builder()
                .id(order.getId())
                .products(products)
                .nettoPrice(order.getNettoPrice())
                .bruttoPrice(order.getBruttoPrice())
                .sentAt(order.getSentAt())
                .deliveredAt(order.getDeliveredAt())
                .client(clientMapper.convertToEntity(order.getClient()))
                .discount(order.getDiscount())
                .build();
    }

    public Order convertToApiModel(OrderEntity orderEntity){
        List<Product> products = orderEntity.getProducts().stream()
                .map(product -> productMapper.convertToApiModel(product))
                .collect(Collectors.toList());

        return new Order()
                .id(orderEntity.getId())
                .products(products)
                .nettoPrice(orderEntity.getNettoPrice())
                .bruttoPrice(orderEntity.getBruttoPrice())
                .sentAt(orderEntity.getSentAt())
                .deliveredAt(orderEntity.getDeliveredAt())
                .client(clientMapper.convertToApiModel(orderEntity.getClient()))
                .discount(orderEntity.getDiscount());
    }
}
