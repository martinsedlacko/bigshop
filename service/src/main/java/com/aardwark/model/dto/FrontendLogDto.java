package com.aardwark.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FrontendLogDto {
    String message;
    String level;
    String stacktrace;

    @Override
    public String toString() {
        return "LoggerDto{" +
                "message='" + message + '\'' +
                ", level='" + level + '\'' +
                ", stacktrace='" + stacktrace + '\'' +
                '}';
    }
}
