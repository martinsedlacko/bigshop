package com.aardwark.service;

import com.aardwark.model.ProductEntity;
import com.aardwark.repository.ProductRepository;
import com.aardwark.exception.ExceptionMessage;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<ProductEntity> findAll(){
        return productRepository.findAll();
    }

    public Optional<ProductEntity> findById(Long id){
        return productRepository.findById(id);
    }

    public void delete(ProductEntity product){
        productRepository.delete(product);
    }

    public ProductEntity save(ProductEntity product){
        return productRepository.save(product);
    }

    public ProductEntity update(ProductEntity updatedProduct) throws NotFoundException {
        if(productRepository.existsById(updatedProduct.getId())){
            return productRepository.save(updatedProduct);
        }else{
            throw new NotFoundException(ExceptionMessage.getNotFoundMessage("Product"));
        }
    }
}
