package com.aardwark.service;

import com.aardwark.model.OrderEntity;
import com.aardwark.repository.OrderRepository;
import com.aardwark.exception.ExceptionMessage;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<OrderEntity> findAll(){
        return orderRepository.findAll();
    }

    public OrderEntity save(OrderEntity order){
        return orderRepository.save(order);
    }

    public void delete(OrderEntity order){
        orderRepository.delete(order);
    }

    public Optional<OrderEntity> findById(Long id) {
        return orderRepository.findById(id);
    }

    public OrderEntity update(OrderEntity orderEntity) throws NotFoundException {
        if(orderRepository.existsById(orderEntity.getId()))
        {
            return orderRepository.save(orderEntity);
        }else {
            throw new NotFoundException(ExceptionMessage.getNotFoundMessage("Order"));
        }
    }
}
