package com.aardwark.service;

import com.aardwark.model.AddressEntity;
import com.aardwark.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<AddressEntity> findAll(){
        return addressRepository.findAll();
    }
}
