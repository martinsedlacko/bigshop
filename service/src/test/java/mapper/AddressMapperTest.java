package mapper;

import com.aardwark.generated.model.Address;
import com.aardwark.mapper.AddressMapper;
import com.aardwark.model.AddressEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AddressMapperTest {
    private Address address;
    private AddressEntity addressEntity;

    private AddressMapper addressMapper;

    @BeforeEach
    void init(){
        addressMapper = new AddressMapper();

        address = new Address()
                .id(1L)
                .city("Sabinov")
                .postCode("08301")
                .street("Matice slovenskej")
                .streetNumber(11)
                .country("Slovakia");

        addressEntity = AddressEntity.builder()
                .id(1L)
                .city("Sabinov")
                .postCode("08301")
                .street("Matice slovenskej")
                .streetNumber(11)
                .country("Slovakia")
                .build();
    }

    @Test
    void givenAddress_whenConvertToEntityModel_thenReturnAddressEntity(){
        AddressEntity result = addressMapper.convertToEntity(address);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(AddressEntity.class, result.getClass()),
                () -> assertEquals(addressEntity.getId(), result.getId()),
                () -> assertEquals(addressEntity.getCity(), result.getCity()),
                () -> assertEquals(addressEntity.getStreet(), result.getStreet()),
                () -> assertEquals(addressEntity.getStreetNumber(), result.getStreetNumber()),
                () -> assertEquals(addressEntity.getPostCode(), result.getPostCode()),
                () -> assertEquals(addressEntity.getCountry(), result.getCountry())
        );
    }

    @Test
    void givenAddressEntity_whenConvertToApiModel_thenReturnAddress(){
        Address result = addressMapper.convertToApiModel(addressEntity);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(Address.class, result.getClass()),
                () -> assertEquals(address.getId(), result.getId()),
                () -> assertEquals(address.getCity(), result.getCity()),
                () -> assertEquals(address.getStreet(), result.getStreet()),
                () -> assertEquals(address.getStreetNumber(), result.getStreetNumber()),
                () -> assertEquals(address.getPostCode(), result.getPostCode()),
                () -> assertEquals(address.getCountry(), result.getCountry())
        );
    }
}
