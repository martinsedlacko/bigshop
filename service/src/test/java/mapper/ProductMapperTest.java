package mapper;

import com.aardwark.generated.model.Product;
import com.aardwark.mapper.ProductMapper;
import com.aardwark.model.ProductEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class ProductMapperTest {

    private Product product;
    private ProductEntity productEntity;

    ProductMapper productMapper;

    @BeforeEach
    void init(){
        productMapper = new ProductMapper();
        product = new Product()
                .id(1L)
                .ammount(10)
                .description("This is the best knife!")
                .discount(new BigDecimal(0))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Knife")
                .price(new BigDecimal(100))
                .type("Kitchen")
                .vat(15.0);

        productEntity = ProductEntity.builder()
                .id(1L)
                .ammount(10)
                .description("This is the best knife!")
                .discount(new BigDecimal(0))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Knife")
                .price(new BigDecimal(100))
                .type("Kitchen")
                .vat(15.0)
                .build();
    }

    @Test
    void givenProduct_whenMapToEntity_thenReturnProductEntity(){
        ProductEntity result = productMapper.convertToEntity(product);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(productEntity.getId(), result.getId()),
                () -> assertEquals(productEntity.getName(), result.getName()),
                () -> assertEquals(productEntity.getAmmount(), result.getAmmount()),
                () -> assertEquals(productEntity.getDescription(), result.getDescription()),
                () -> assertEquals(productEntity.getDiscount(), result.getDiscount()),
                () -> assertArrayEquals(productEntity.getImage(), result.getImage()),
                () -> assertEquals(productEntity.getPrice(), result.getPrice()),
                () -> assertEquals(productEntity.getType(), result.getType())
        );
    }

    @Test
    void givenProductEntity_whenMapToApiModel_thenReturnProduct(){
        Product result = productMapper.convertToApiModel(this.productEntity);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(product.getId(), result.getId()),
                () -> assertEquals(product.getName(), result.getName()),
                () -> assertEquals(product.getAmmount(), result.getAmmount()),
                () -> assertEquals(product.getDescription(), result.getDescription()),
                () -> assertEquals(product.getDiscount(), result.getDiscount()),
                () -> assertArrayEquals(product.getImage(), result.getImage()),
                () -> assertEquals(product.getPrice(), result.getPrice()),
                () -> assertEquals(product.getType(), result.getType())
        );
    }
}
