package mapper;

import com.aardwark.generated.model.Address;
import com.aardwark.generated.model.Client;
import com.aardwark.generated.model.Order;
import com.aardwark.generated.model.Product;
import com.aardwark.mapper.*;
import com.aardwark.model.AddressEntity;
import com.aardwark.model.ClientEntity;
import com.aardwark.model.OrderEntity;
import com.aardwark.model.ProductEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderMapperTest {

    private Order order;
    private OrderEntity orderEntity;
    private OrderMapper orderMapper;

    @BeforeEach
    void init(){
        this.orderMapper = new OrderMapper(new ClientMapper(new AddressMapper()), new ProductMapper());

        Product product = new Product()
                .id(1L)
                .ammount(10)
                .description("This is the best knife!")
                .discount(new BigDecimal(0))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Knife")
                .price(new BigDecimal(100))
                .type("Kitchen")
                .vat(15.0);

        ProductEntity productEntity = ProductEntity.builder()
                .id(1L)
                .ammount(10)
                .description("This is the best knife!")
                .discount(new BigDecimal(0))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Knife")
                .price(new BigDecimal(100))
                .type("Kitchen")
                .vat(15.0)
                .build();

        AddressEntity addressEntity = AddressEntity.builder()
                .id(1L)
                .city("Presov")
                .postCode("08754")
                .street("Sabinovska")
                .streetNumber(45)
                .country("Slovakia")
                .build();

        Address address = new Address()
                .id(1L)
                .city("Presov")
                .postCode("08754")
                .street("Sabinovska")
                .streetNumber(45)
                .country("Slovakia");

        Client client = new Client()
                .id(1L)
                .email("test2@gmail.com")
                .lastName("Tester")
                .firstName("Peter")
                .telephoneNumber("+421879357554")
                .billingAddress(address)
                .deliveryAddress(address);

        ClientEntity clientEntity = ClientEntity.builder()
                .id(1L)
                .email("test2@gmail.com")
                .lastName("Tester")
                .firstName("Peter")
                .telephoneNumber("+421879357554")
                .billingAddress(addressEntity)
                .deliveryAddress(addressEntity)
                .build();

        this.order = new Order()
                .id(1L)
                .products(List.of(product))
                .nettoPrice(new BigDecimal(100))
                .bruttoPrice(new BigDecimal(120))
                .sentAt("")
                .deliveredAt("")
                .discount(new BigDecimal(0))
                .client(client);

        this.orderEntity = OrderEntity.builder()
                .id(1L)
                .products(List.of(productEntity))
                .nettoPrice(new BigDecimal(100))
                .bruttoPrice(new BigDecimal(120))
                .sentAt("")
                .deliveredAt("")
                .discount(new BigDecimal(0))
                .client(clientEntity)
                .build();
    }

    @Test
    void givenClient_WhenConvertToEntityModel_ReturnClientEntity(){
        OrderEntity result = this.orderMapper.convertToEntity(order);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(OrderEntity.class, result.getClass()),
                () -> assertEquals(orderEntity.getId(), result.getId()),
                () -> assertEquals(orderEntity.getProducts(), result.getProducts()),
                () -> assertEquals(orderEntity.getNettoPrice(), result.getNettoPrice()),
                () -> assertEquals(orderEntity.getBruttoPrice(), result.getBruttoPrice()),
                () -> assertEquals(orderEntity.getSentAt(), result.getSentAt()),
                () -> assertEquals(orderEntity.getDeliveredAt(), result.getDeliveredAt()),
                () -> assertEquals(orderEntity.getDiscount(), result.getDiscount()),
                () -> assertEquals(orderEntity.getClient().getClass(), result.getClient().getClass()),
                () -> assertEquals(orderEntity.getClient(), result.getClient()),
                () -> assertEquals(orderEntity.getClient().getBillingAddress().getClass(),
                        result.getClient().getBillingAddress().getClass()),
                () -> assertEquals(orderEntity.getClient().getBillingAddress(),
                        result.getClient().getBillingAddress()),
                () -> assertEquals(orderEntity.getClient().getDeliveryAddress().getClass(),
                        result.getClient().getDeliveryAddress().getClass()),
                () -> assertEquals(orderEntity.getClient().getDeliveryAddress(),
                        result.getClient().getDeliveryAddress())
                );
    }

    @Test
    void givenClientEntity_WhenConvertToApiModel_ReturnClient(){
        Order result = this.orderMapper.convertToApiModel(orderEntity);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(Order.class, result.getClass()),
                () -> assertEquals(order.getId(), result.getId()),
                () -> assertEquals(order.getProducts(), result.getProducts()),
                () -> assertEquals(order.getNettoPrice(), result.getNettoPrice()),
                () -> assertEquals(order.getBruttoPrice(), result.getBruttoPrice()),
                () -> assertEquals(order.getSentAt(), result.getSentAt()),
                () -> assertEquals(order.getDeliveredAt(), result.getDeliveredAt()),
                () -> assertEquals(order.getDiscount(), result.getDiscount()),
                () -> assertEquals(order.getClient().getClass(), result.getClient().getClass()),
                () -> assertEquals(order.getClient(), result.getClient()),
                () -> assertEquals(order.getClient().getBillingAddress().getClass(),
                        result.getClient().getBillingAddress().getClass()),
                () -> assertEquals(order.getClient().getBillingAddress(),
                        result.getClient().getBillingAddress()),
                () -> assertEquals(order.getClient().getDeliveryAddress().getClass(),
                        result.getClient().getDeliveryAddress().getClass()),
                () -> assertEquals(order.getClient().getDeliveryAddress(),
                        result.getClient().getDeliveryAddress())
        );
    }


}
