package mapper;

import com.aardwark.generated.model.Address;
import com.aardwark.generated.model.Client;
import com.aardwark.mapper.AddressMapper;
import com.aardwark.mapper.ClientMapper;
import com.aardwark.model.AddressEntity;
import com.aardwark.model.ClientEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ClientMapperTest {

    private Client client;
    private ClientEntity clientEntity;

    private ClientMapper clientMapper;
    private final AddressMapper addressMapper = new AddressMapper();

    @BeforeEach
    void init(){
        clientMapper = new ClientMapper(addressMapper);

        AddressEntity addressEntity = AddressEntity.builder()
                .id(1L)
                .city("Presov")
                .postCode("08754")
                .street("Sabinovska")
                .streetNumber(45)
                .country("Slovakia")
                .build();

        Address address = new Address()
                .id(1L)
                .city("Presov")
                .postCode("08754")
                .street("Sabinovska")
                .streetNumber(45)
                .country("Slovakia");

        client = new Client()
                .id(1L)
                .email("test2@gmail.com")
                .lastName("Tester")
                .firstName("Peter")
                .telephoneNumber("+421879357554")
                .billingAddress(address)
                .deliveryAddress(address);

        clientEntity = ClientEntity.builder()
                .id(1L)
                .email("test2@gmail.com")
                .lastName("Tester")
                .firstName("Peter")
                .telephoneNumber("+421879357554")
                .billingAddress(addressEntity)
                .deliveryAddress(addressEntity)
                .build();
    }

    @Test
    void givenClient_whenConvertToEntityModel_thenReturnClientEntity(){
        ClientEntity result = clientMapper.convertToEntity(client);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(ClientEntity.class, result.getClass()),
                () -> assertEquals(clientEntity.getId(), result.getId()),
                () -> assertEquals(clientEntity.getEmail(), result.getEmail()),
                () -> assertEquals(clientEntity.getLastName(), result.getLastName()),
                () -> assertEquals(clientEntity.getFirstName(), result.getFirstName()),
                () -> assertEquals(clientEntity.getTelephoneNumber(), result.getTelephoneNumber()),
                () -> assertEquals(AddressEntity.class, result.getDeliveryAddress().getClass()),
                () -> assertEquals(AddressEntity.class, result.getBillingAddress().getClass()),
                () -> assertEquals(clientEntity.getDeliveryAddress(), result.getDeliveryAddress()),
                () -> assertEquals(clientEntity.getBillingAddress(), result.getBillingAddress())
        );
    }

    @Test
    void givenClientEntity_whenConvertToApiModel_thenReturnClient(){
        Client result = clientMapper.convertToApiModel(clientEntity);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(Client.class, result.getClass()),
                () -> assertEquals(client.getId(), result.getId()),
                () -> assertEquals(client.getEmail(), result.getEmail()),
                () -> assertEquals(client.getLastName(), result.getLastName()),
                () -> assertEquals(client.getFirstName(), result.getFirstName()),
                () -> assertEquals(client.getTelephoneNumber(), result.getTelephoneNumber()),
                () -> assertEquals(Address.class, result.getDeliveryAddress().getClass()),
                () -> assertEquals(Address.class, result.getBillingAddress().getClass()),
                () -> assertEquals(client.getDeliveryAddress(), result.getDeliveryAddress()),
                () -> assertEquals(client.getBillingAddress(), result.getBillingAddress())
        );
    }
}
