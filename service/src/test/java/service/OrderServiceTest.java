package service;

import com.aardwark.model.ClientEntity;
import com.aardwark.model.OrderEntity;
import com.aardwark.repository.OrderRepository;
import com.aardwark.service.OrderService;
import com.aardwark.exception.ExceptionMessage;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.SpringBootConfiguration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@SpringBootConfiguration
class OrderServiceTest {

    private OrderService orderService;
    private final List<OrderEntity> orders = new ArrayList<>();

    @Mock
    OrderRepository orderRepository;

    @BeforeEach
    void init(){
        orderService = new OrderService(orderRepository);

        ClientEntity client = ClientEntity.builder()
                .id(1L)
                .email("test@gm.com")
                .firstName("Martin")
                .lastName("Test")
                .telephoneNumber("+421907777778")
                .build();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date = LocalDateTime.of(LocalDate.of(2020, 2, 13),
                LocalTime.of(11, 30, 0));

        OrderEntity orderEntity1 = OrderEntity.builder()
                .id(1L)
                .bruttoPrice(new BigDecimal(100))
                .deliveredAt(date.format(formatter))
                .discount(new BigDecimal(10))
                .nettoPrice(new BigDecimal(80))
                .sentAt(date.minusDays(2).format(formatter))
                .client(client)
                .build();

        date = LocalDateTime.of(LocalDate.of(2018, 8, 21),
                LocalTime.of(18, 0, 0));
        OrderEntity orderEntity2 = OrderEntity.builder()
                .id(2L)
                .bruttoPrice(new BigDecimal(580))
                .deliveredAt(date.format(formatter))
                .discount(new BigDecimal(20))
                .nettoPrice(new BigDecimal(500))
                .sentAt(date.minusDays(4).format(formatter))
                .client(client)
                .build();

        orders.addAll(List.of(orderEntity1, orderEntity2));
    }

    @Test
    void givenVoid_whenFindAll_thenReturnAllOrders(){
        when(orderRepository.findAll())
                .thenReturn(orders);

        List<OrderEntity> result = orderService.findAll();

        assertAll(
                () -> assertEquals(orders.size(), result.size()),
                () -> assertEquals(orders.get(0), result.get(0)),
                () -> assertEquals(orders.get(1), result.get(1)),
                () -> assertEquals(orders.get(0).getClient(), result.get(0).getClient())
        );

        verify(orderRepository, times(1)).findAll();
    }

    @Test
    void givenNewOrderDto_whenSave_thenSuccess(){
        OrderEntity order = orders.get(0);

        when(orderRepository.save(order))
                .thenReturn(order);

        OrderEntity result = orderService.save(order);

        assertAll(
                () -> assertEquals(order, result),
                () -> assertNotNull(result.getId())
        );

        verify(orderRepository, times(1)).save(order);
    }

    @Test
    void givenOrder_whenDelete_thenSuccess(){
        OrderEntity order = orders.get(0);

        orderService.delete(order);

        verify(orderRepository, times(1)).delete(order);
    }

    @Test
    void givenCorrectId_whenFindById_thenReturnOrder(){
        Long id = orders.get(0).getId();

        when(orderRepository.findById(id))
                .thenReturn(Optional.of(orders.get(0)));

        Optional<OrderEntity> result = orderService.findById(id);

        assertAll(
                () -> assertNotNull(result),
                () -> assertTrue(result.isPresent()),
                () -> assertEquals(orders.get(0), result.orElse(null))
        );

        verify(orderRepository, times(1)).findById(1L);
    }

    @Test
    void givenIncorrectId_whenFindById_thenReturnEmpty(){
        when(orderRepository.findById(999L))
                .thenReturn(Optional.empty());

        Optional<OrderEntity> result = orderService.findById(999L);

        assertAll(
                () -> assertNotNull(result),
                () -> assertFalse(result.isPresent()),
                () -> assertEquals(Optional.empty(), result)
        );

        verify(orderRepository, times(1)).findById(999L);
    }

    @Test
    void givenUpdatedOrder_whenUpdate_thenReturnUpdatedOrder() {
        OrderEntity updatedOrder = orders.get(0);
        BigDecimal newBruttoPrice = new BigDecimal(8500);

        updatedOrder.setBruttoPrice(newBruttoPrice);

        when(orderRepository.existsById(updatedOrder.getId()))
                .thenReturn(true);
        when(orderRepository.save(updatedOrder))
                .thenReturn(updatedOrder);

        AtomicReference<OrderEntity> result = new AtomicReference<>();

        assertAll(
                () -> assertDoesNotThrow(() -> result.set(orderService.update(updatedOrder))),
                () -> assertNotNull(result),
                () -> assertEquals(newBruttoPrice, result.get().getBruttoPrice())
        );

        verify(orderRepository, times(1)).save(updatedOrder);
        verify(orderRepository, times(1)).existsById(updatedOrder.getId());
    }

    @Test
    void givenNewOrder_whenUpdate_thenThrowNotFoundException() {
        OrderEntity orderEntity = orders.get(0);
        orderEntity.setId(null);

        when(orderRepository.existsById(orderEntity.getId()))
                .thenReturn(false);

        NotFoundException result = assertThrows(NotFoundException.class, () -> orderService.update(orderEntity));

        assertAll(
                () -> assertEquals(ExceptionMessage.getNotFoundMessage("Order"), result.getMessage()),
                () -> assertEquals(NotFoundException.class, result.getClass())
        );

        verify(orderRepository, times(0)).save(orderEntity);
        verify(orderRepository, times(1)).existsById(orderEntity.getId());
    }
}
