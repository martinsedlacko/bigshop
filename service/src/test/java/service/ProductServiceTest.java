package service;

import com.aardwark.model.ProductEntity;
import com.aardwark.repository.ProductRepository;
import com.aardwark.service.ProductService;
import com.aardwark.exception.ExceptionMessage;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    ProductService productService;
    List<ProductEntity> products = new ArrayList<>();

    @Mock
    ProductRepository productRepository;



    @BeforeEach
    void init(){
        productService = new ProductService(productRepository);

        ProductEntity product1 = ProductEntity.builder()
                .id(1L)
                .ammount(10)
                .description("This is the best knife!")
                .discount(new BigDecimal(0))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Knife")
                .price(new BigDecimal(100))
                .type("Kitchen")
                .vat(15.0)
                .build();

        ProductEntity product2 = ProductEntity.builder()
                .id(2L)
                .ammount(1)
                .description("This is the best spoon!")
                .discount(new BigDecimal(10))
                .image("Image".getBytes(StandardCharsets.UTF_8))
                .name("Spoon")
                .price(new BigDecimal(20))
                .type("Kitchen")
                .vat(15.0)
                .build();

        products.addAll(List.of(product1, product2));
    }

    @Test
    void givenVoid_whenFindAll_returnAllProducts() {
        when(productRepository.findAll()).thenReturn(products);

        List<ProductEntity> result = productService.findAll();

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(products.size(), result.size()),
                () -> assertEquals(products.get(0), result.get(0)),
                () -> assertEquals(products.get(1), result.get(1))
        );

        verify(productRepository, times(1)).findAll();
    }

    @Test
    void givenCorrectId_whenFindById_returnProduct(){
        Long correctId = products.get(0).getId();

        when(productRepository.findById(correctId))
                .thenReturn(Optional.of(products.get(0)));

        Optional<ProductEntity> result = productService.findById(correctId);

        assertAll(
                () -> assertNotNull(result),
                () -> assertTrue(result.isPresent()),
                () -> assertEquals(products.get(0), result.orElse(null))
        );

        verify(productRepository, times(1)).findById(correctId);
    }

    @Test
    void givenIncorrectId_whenFindById_returnEmpty(){
        Long incorrectId = 999L;

        when(productRepository.findById(incorrectId))
                .thenReturn(Optional.empty());

        Optional<ProductEntity> result = productService.findById(incorrectId);

        assertAll(
                () -> assertNotNull(result),
                () -> assertFalse(result.isPresent()),
                () -> assertTrue(result.isEmpty())
        );

        verify(productRepository, times(1)).findById(incorrectId);
    }

    @Test
    void givenProduct_whenDelete_thenSuccess(){
        ProductEntity product = products.get(0);

        productService.delete(product);

        verify(productRepository, times(1)).delete(product);
    }

    @Test
    void givenNewProduct_whenSave_thenReturnNewProduct(){
        ProductEntity product = products.get(0);

        when(productRepository.save(product))
                .thenReturn(product);

        ProductEntity result = productService.save(product);

        assertAll(
                () -> assertNotNull(result),
                () -> assertEquals(product, result)
        );

        verify(productRepository, times(1)).save(product);
    }

    @Test
    void givenUpdatedProduct_whenUpdate_thenReturnUpdatedProduct(){
        ProductEntity updatedProduct = products.get(0);
        updatedProduct.setName("Fork");

        when(productRepository.save(updatedProduct))
                .thenReturn(updatedProduct);
        when(productRepository.existsById(updatedProduct.getId()))
                .thenReturn(true);

        AtomicReference<ProductEntity> result = new AtomicReference<>();

        assertAll(
                () -> assertDoesNotThrow(() -> result.set(productService.update(updatedProduct))),
                () -> assertNotNull(result),
                () -> assertNotNull(result.get()),
                () -> assertEquals(updatedProduct.getName(), result.get().getName())
        );

        verify(productRepository, times(1)).save(updatedProduct);
    }

    @Test
    void givenNewProduct_whenUpdate_thenThrowNotFoundException(){
        Long incorrectId = 999L;
        ProductEntity newProduct = products.get(0);
        newProduct.setId(incorrectId);

        when(productRepository.existsById(newProduct.getId()))
                .thenReturn(false);

        NotFoundException exception = assertThrows(NotFoundException.class,() -> productService.update(newProduct));

        assertAll(
                () -> assertEquals(NotFoundException.class, exception.getClass()),
                () -> assertEquals(ExceptionMessage.getNotFoundMessage("Product"), exception.getMessage())
        );

        verify(productRepository, times(1)).existsById(newProduct.getId());
        verify(productRepository, times(0)).save(newProduct);
    }
}
